// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:
  {
    apiKey: "AIzaSyBK2u_MNQJMKqiwAVuLe_QYWj48iEjVqJs",
    authDomain: "fbreg-290ed.firebaseapp.com",
    databaseURL: "https://fbreg-290ed.firebaseio.com",
    projectId: "fbreg-290ed",
    storageBucket: "fbreg-290ed.appspot.com",
    messagingSenderId: "88546717811"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
